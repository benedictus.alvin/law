from django.contrib import admin
from django.urls import path
from django.conf.urls import url, include
from django.views.generic.base import RedirectView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('metaWeather/', include('metaWeather.urls')),
    path('',RedirectView.as_view(url="/metaWeather/",permanent="true")),
]
