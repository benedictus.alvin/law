from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from urllib.request import urlopen
import requests
import datetime
def index(request):
	response = {}
	location = request.GET.get('location', None)
	if location == None:
		return render(request, 'metaWeather.html', response)
	else:
		loc_url = requests.get("https://www.metaweather.com/api/location/search/?query=" + location)
		try:
			loc_id = loc_url.json()[0]['woeid']
			weather_url = requests.get("https://www.metaweather.com/api/location/" + str(loc_id))
			weather = weather_url.json()
			weather_state_name = weather["consolidated_weather"][0]["weather_state_name"]
			weather_state_abbr = weather["consolidated_weather"][0]["weather_state_abbr"]
			weather_state_logo = "https://www.metaweather.com//static/img/weather/png/" + weather_state_abbr + ".png"
			applicable_date = weather["consolidated_weather"][0]["applicable_date"]
			the_temp = weather["consolidated_weather"][0]["the_temp"]
			wind_speed = weather["consolidated_weather"][0]["wind_speed"]
			air_pressure = weather["consolidated_weather"][0]["air_pressure"]
			humidity = weather["consolidated_weather"][0]["humidity"]
			predictability = weather["consolidated_weather"][0]["predictability"]
			response['location'] = location
			response['weather_state_name'] = weather_state_name
			response['weather_state_logo'] = weather_state_logo
			response['applicable_date'] = datetime.datetime.strptime(applicable_date, '%Y-%m-%d').strftime('%m/%d/%Y')
			response['the_temp'] = round(the_temp, 2)
			response['wind_speed'] = round(wind_speed, 2)
			response['air_pressure'] = air_pressure
			response['humidity'] = humidity
			response['predictability'] = predictability
			return render(request, 'metaWeather.html', response)
		except:
			response['no_location'] = location +" not found" 
			return render(request, 'metaWeather.html', response)