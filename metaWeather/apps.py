from django.apps import AppConfig


class MetaweatherConfig(AppConfig):
    name = 'metaWeather'
